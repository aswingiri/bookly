<?php
Route::get('/features', 'PageController@features')->name('features');
Route::get('/pricing', 'PageController@pricing')->name('pricing');
Route::get('/integration', 'PageController@integration')->name('integration');
Route::get('/solutions', 'PageController@solutions')->name('solutions');
Route::get('/for-team', 'PageController@for_team')->name('for_team');