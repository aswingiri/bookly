<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/dashboard', function () {
    return view('layouts.dashboard');
})->middleware('auth');

Auth::routes();

// include admin route file
include_once base_path('routes/web/admin.php');
// include supplier route file
include_once base_path('routes/web/tenant.php');
// include member route file
include_once base_path('routes/web/guest.php');

Route::get('/features', 'PageController@features')->name('features');
Route::get('/pricing', 'PageController@pricing')->name('pricing');
Route::get('/integration', 'PageController@integration')->name('integration');
Route::get('/solutions', 'PageController@solutions')->name('solutions');
Route::get('/for-team', 'PageController@for_team')->name('for_team');
