<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    //
    function features(){
    	return view( 'pages.features' );
    }

    function pricing(){
    	return view( 'pages.pricing' );
    }

    function integration(){
    	return view( 'pages.integration' );
    }
}
