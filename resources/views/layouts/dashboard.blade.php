@extends('layouts.app')
@section('content')
<div class="top-section">
          <div class="container">
            <div class="row">
              
              <div class="col-sm-5">
                <h2 class="page-heading">Dashboard</h2>
              </div>
              
              <div class="col-sm-7"></div>
              
            </div>
          </div>
</div>
      
<div class="main-content">
  <div class="container">
          @hasSection('main')
            @yield('main')
          @endif
          
  </div>
</div>
@endsection