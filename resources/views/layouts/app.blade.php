<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>
@hasSection('title')
    @yield('title')
@else
    {{ config('app.name', 'Laravel') }}
@endif
</title>

@include('partials.layout.main-css')

</head>
<body>

    <header id="main-header">
        @include('partials.layout.nav')
    </header>
  
    <section class="body-content" style="min-height:500px;">
      @yield('content')
    </section>
    <footer id="main-footer">
        <div class="container">
            <span>&copy; Copyright 2018.</span> <span><a href="#">Privacy policy</a> | <a href="#">Term &amp; Conditions</a></span>
             
        </div>
    </footer>
@include('partials.layout.main-scripts')
</body>
</html>
