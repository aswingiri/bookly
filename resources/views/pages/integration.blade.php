@extends('layouts.app')

@section('content')
<div class="main-content">
    <div class="container">
    	<div class="section-header">
            <h2 class="dark-text">Integartion</h2>
            <h6>
                We design & develop qaulity products to help small & medium level business.
            </h6>
        </div>

        <div class="row">
            <div class="col-md-4 col-sm-4">
                <div class="feature">
                    <div class="feature-icon">
                       <i class="fa fa-cogs" aria-hidden="true"></i>
                    </div>
                    <h5>Exchange of goods and services</h5>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    </p>
                </div>

                <div class="feature">
                    <div class="feature-icon">
                        <i class="fa fa-line-chart" aria-hidden="true"></i>
                    </div>
                    <h5>Business Profit </h5>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    </p>
                </div>
            </div>

            <div class="col-md-4 col-sm-4">
                <div class="feature">
                    <div class="feature-icon">
                        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                    </div>
                    <h5>Risks and Uncertainties</h5>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    </p>
                </div>

                <div class="feature">
                    <div class="feature-icon">
                        <i class="fa fa-handshake-o" aria-hidden="true"></i>
                    </div>
                    <h5>Buyer and Seller</h5>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    </p>
                </div>
            </div>
            <div class="col-md-4 col-sm-4">
                <div class="feature">
                    <div class="feature-icon">
                        <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                    </div>
                    <h5>Risks and Uncertainties</h5>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    </p>
                </div>

                <div class="feature">
                    <div class="feature-icon">
                        <i class="fa fa-handshake-o" aria-hidden="true"></i>
                    </div>
                    <h5>Buyer and Seller</h5>
                    <p>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection