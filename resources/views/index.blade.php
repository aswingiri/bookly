@extends('layouts.app')
@section('content')
<div class="top-section">
          <div class="container">
            <div class="row">
              
              <div class="col-sm-4">
                <h2 class="page-heading">Dashboard</h2>
              </div>
              
              <div class="col-sm-4"></div>
              <div class="col-sm-4 text-right">
                  <a href="">My Calendar</a>
              </div>
              
            </div>
          </div>
</div>
      
<div class="main-content">
  <div class="container">
      <div class="event-list">
          <div class="row">
            <div class="col-md-3">
              <div class="event">
                <button class="btn btn-light btn-block">Event 1</button>
              </div>
            </div>
            <div class="col-md-3">
              <div class="event">
                <button class="btn btn-light btn-block">Event 2</button>
              </div>
            </div>
            <div class="col-md-3">
              <div class="event">
                <button class="btn btn-light btn-block">Event 3</button>
              </div>
            </div>
            <div class="col-md-3">
              <div class="event">
                <button class="btn btn-secondary btn-block">More &raquo;</button>
              </div>
            </div>
          </div>
      </div>
      <hr/>
      <div class="calendar">
        List of recent appointments with ajax filter option
      </div>
  </div>
</div>
@endsection