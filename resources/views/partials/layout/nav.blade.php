<nav id="main-nav" class="navbar navbar-expand-lg navbar-dark">
          <a class="navbar-brand" href="#">
            <img src="{{asset('assets/images/logo.png')}}" height="30"/>
          </a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            
            
            <ul class="navbar-nav ml-auto">
              
              @if(auth()->user())
              <li class="nav-item active">
                <a class="nav-link" href="#">Dashboard <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Event types</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#">Help</a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fa fa-user"></i>bookly.com/username
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                  <a class="dropdown-item" href="#">Settings</a>
                  <a class="dropdown-item" href="#">Billing</a>
                  <a class="dropdown-item" href="#">Account settings</a>
                  <a class="dropdown-item" href="#">Calendar connect</a>
                  <a class="dropdown-item" href="{{url('logout')}}">Logout</a>
                </div>
              </li>
              @else
                <li class="nav-item">
                  <a class="nav-link" href="{{url('login')}}">Login</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{url('register')}}">Register</a>
                </li>
              @endif
            </ul>
            
            
          </div>
</nav>